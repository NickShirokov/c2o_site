<?php

namespace frontend\models;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class ContactForm extends Model
{
    public $name;
    public $email;
    public $phone;
    public $subject;
    public $body;
    public $verifyCode;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // name, email, subject and body are required
            [['name', 'email'], 'required'],
            // email has to be a valid email address
            ['email', 'email'],
            // verifyCode needs to be entered correctly
            //['verifyCode', 'captcha'],
            [['verifyCode', 'subject', 'body'], 'safe'],
            [['phone'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'verifyCode' => 'Verification Code',   
			'phone' => Yii::t('app', 'Тел'),
            'email' => Yii::t('app', 'Email'),
            'name' => Yii::t('app', 'Имя'),
			
			
        ];
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     *
     * @param string $email the target email address
     * @return bool whether the email was sent
     */
    public function sendEmail($email)
    {
		$this->subject = 'Заявка с сайта c2o.me';
		
        return Yii::$app->mailer->compose()
            ->setTo($email)
            ->setFrom(['info@c2o.me' => 'Сайт c2o.me'])
            ->setSubject($this->subject)
            ->setTextBody('')
			->setHtmlBody('Имя: '.$this->name. '<br>'.'Email: '.$this->email.'<br>'.'Телефон: '.$this->phone)
            
            ->send();
    }
}
