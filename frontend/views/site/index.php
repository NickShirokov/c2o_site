<?php

/* @var $this yii\web\View */

use yii\helpers\Url;
use yii\bootstrap\Modal;

$this->title = 'Click to Offer - высокотехнологичная платформа для
	   коммуникации, информирования и сбора задолженностей с ваших клиентов';
?>



<div class="site-index">




<section id="" class="first-banner">


    <div class="container flex-center-align">

    <div class="jumbotron">
       

	   <h1>Технологии взыскания и сбора<br> платежей, меняющие правила</h1>

	   <p class="lead">
	   
	   Click to Offer - высокотехнологичная платформа для коммуникации, <br>
		информирования и сбора задолженности с ваших клиентов

	   
	   </p>

       

	   <img style="width: 70%;" class="" src="images/1.png">
	   

	   </div>

	
	</div>
	
</section>	
	
	
	
<!-- ВТорая секция -->	
	
<section id="" class="second-sec">	
<div class="container">	
	
	
	<div class="row">
	
		<div class="col-md-6 img-box-wr">
			<img style="width: 75%;" src="images/2.png">
		</div>
	
	
		<div class="col-md-6">
			
			
			<p class="blue-text">Решение</p>
			<h2>Предприятия ЖКХ и поставщики энергоресурсов</h2>
			
			<p class="lead">
		Эффективные инструменты для дистанционных платежей,  взыскания задолженности по квартплате и коммунальным услугам
		</p>
			
			<div class="row">
			
				<div class="col-md-6">
					
					<ul class="main-ul">
						<li>Полное соответствие требованиям правилами предоставления коммунальных услуг № 354, от 06.05.2011 г.
						</li>
					
						<li>
							Инструменты реструктуризации долга, пролонгации, реализация сценария обещанных платежей
						</li>
					
						<li>
						
							Интеграция платежного сервиса с возможностью совершения платежей онлайн
						</li>
					</ul>
					
				
				
				</div>
				
				<div class="col-md-6">
					<ul class="main-ul">
					
						<li>
						Легкий доступ клиента к данным личного кабинета информация о задолженности и возможности погашения


						
						</li>
						
						<li>
							Мультимедиа взаимодействие: баннеры, видео, фото, динамические документы / досудебные претензии

						
						</li>
						
						<li>
							С2О с применением голосового сервиса (+IVR), меняет сценарий информирования в зависимости от действий заемщика

						
						</li>
						
					
					</ul>	
				</div>
				
				<div class="clear"></div>
				<hr>
				
				<div class="col-md-12">
				
				<p class="text-right blue-text"><a href="<?php Url::base(''); ?>/zkh/">
				
				Узнать подробнее 
				
				<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 62.1 116.09"><defs><style>.cls-1{fill:#0062ff;}</style></defs><title>Asset 1</title><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><path class="cls-1" d="M1.2,1.19A4.1,4.1,0,0,0,1.2,7L52.3,58,1.2,109.09a4.1,4.1,0,1,0,5.8,5.8L60.9,61a4,4,0,0,0,1.2-2.9,4.18,4.18,0,0,0-1.2-2.9L7,1.29A4,4,0,0,0,1.2,1.19Z"/></g></g></svg>
				
				</a></p>
				</div>
			</div>
			
			
		</div>
	
	
	
	
	</div>
	
	
	
</div>	
</section>	
	
	
	
	
	
	
<!-- Третья секция -->	
	
<section id="" class="third-sec">	
<div class="container">	
	
	
	<div class="row">
	
		
		<div class="col-md-6 mob-block">
			<img style="width: 100%;" src="images/3.png">
		</div>	
	
	
	
		<div class="col-md-6">
			
			
			<p class="blue-text">Решение</p>
			<h2>Коллекторские агентства</h2>
			
			<p class="lead">
			Эффективный сбор задолженности и инструменты для побуждения к выплатам должников (досудебные претензии, прощение части задолженности).
</p>
			
			<div class="row">
			
				<div class="col-md-6">
					
					<ul class="main-ul">
						<li>
							Полное соответствие требованиям 230 – ФЗ и законодательству о персональных данных

						</li>
					
						<li>
							Инструменты реструктуризации долга, пролонгации, реализация сценария обещанных платежей
	</li>
					
						<li>
						
						Интеграция платежного сервиса с возможностью совершения платежей онлайн	</li>
					</ul>
					
				
				
				</div>
				
				<div class="col-md-6">
					<ul class="main-ul">
					
						<li>
						Легкий доступ клиента к данным личного кабинета информация о задолженности и возможности погашения


						
						</li>
						
						<li>
							Мультимедиа взаимодействие: баннеры, видео, фото, динамические документы / досудебные претензии

						
						</li>
						
						<li>
							С2О с применением голосового сервиса (+IVR), меняет сценарий информирования в зависимости от действий заемщика


						</li>
						
					
					</ul>	
				</div>
				<div class="clear"></div>
				<hr>
				<div class="col-md-12">
				<p class="text-right blue-text"><a href="<?php Url::base(''); ?>/collector/">Узнать подробнее 
				
				
				<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 62.1 116.09"><defs><style>.cls-1{fill:#0062ff;}</style></defs><title>Asset 1</title><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><path class="cls-1" d="M1.2,1.19A4.1,4.1,0,0,0,1.2,7L52.3,58,1.2,109.09a4.1,4.1,0,1,0,5.8,5.8L60.9,61a4,4,0,0,0,1.2-2.9,4.18,4.18,0,0,0-1.2-2.9L7,1.29A4,4,0,0,0,1.2,1.19Z"/></g></g></svg>
				
				
				</a></p>
			
			</div>
			</div>
			
			
		</div>
		
		<div class="col-md-6 mob-none">
			<img style="width: 100%;" src="images/3.png">
		</div>	
	</div>

</div>	
</section>	







<!-- Четвертая секция -->	
	
<section id="" class="second-sec">	
<div class="container">	
	
	
	<div class="row">
	
		<div class="col-md-6">
			<img style="width: 75%;" src="images/4.png">
		</div>
	
	
		<div class="col-md-6">
			
			
			<p class="blue-text">Решение</p>
			<h2>Банковский сектор</h2>
			
			<p class="lead">
		Организация быстрой коммуникации с клиентом, удобная оплата кредитных продуктов и сбор задолженности. Инструменты продвижения кроссмаркетинговых программ и система допродаж
		</p>
			
			<div class="row">
			
				<div class="col-md-6">
					
					<ul class="main-ul">
						<li>
						Полное соответствие требованиям 230 – ФЗ и законодательству о персональных данных

						
						</li>
					
						<li>
						Инструменты реструктуризации долга, пролонгации, реализация сценария обещанных платежей
	</li>
					
						<li>
						
					Интеграция платежного сервиса с возможностью совершения платежей онлайн
	</li>
					</ul>
					
				
				
				</div>
				
				<div class="col-md-6">
					<ul class="main-ul">
					
						<li>
						Легкий доступ клиента к данным личного кабинета информация о задолженности и возможности погашения

						
						</li>
						
						<li>
							Полноценная интеграция системы с АССЗ и АБС комплексами

						
						</li>
						
						<li>
							С2О с применением голосового сервиса (+IVR), меняет сценарий информирования в зависимости от действий заемщика

						
						</li>
						
					
					</ul>	
				</div>
				<div class="clear"></div>
				<hr>
				<div class="col-md-12">
				<p class="text-right blue-text"><a href="<?php Url::base(''); ?>/bank/">
				
				Узнать подробнее 
				<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 62.1 116.09"><defs><style>.cls-1{fill:#0062ff;}</style></defs><title>Asset 1</title><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><path class="cls-1" d="M1.2,1.19A4.1,4.1,0,0,0,1.2,7L52.3,58,1.2,109.09a4.1,4.1,0,1,0,5.8,5.8L60.9,61a4,4,0,0,0,1.2-2.9,4.18,4.18,0,0,0-1.2-2.9L7,1.29A4,4,0,0,0,1.2,1.19Z"/></g></g></svg>
				
				
				</a></p>
				</div>
			</div>
			
			
		</div>
	
	
	
	
	</div>
	
	
	
</div>	
</section>	
	
	
	
	
	
	
<!-- Пятая секция -->	
	
<section id="" class="third-sec">	
<div class="container">	
	
	
	<div class="row">
	
		<div class="col-md-6 mob-block">
			<img style="width: 100%;" src="images/5.png">
		</div>	
	
	
	
		<div class="col-md-6">
			
			
			<p class="blue-text">Решение</p>
			<h2>Микрофинансовые организации</h2>
			
			<p class="lead">
			
			Полноценный контроль собираемости задолжностей и система информирования групп с приближающимся сроком выплаты по кредиту.

			
			</p>
			
			<div class="row">
			
				<div class="col-md-6">
					
					<ul class="main-ul">
						<li>
							Полное соответствие требованиям 230 – ФЗ, 152 – ФЗ и соблюдение закона о персональных данных

						</li>
					
						<li>
							
Инструменты реструктуризации долга, пролонгации, реализация сценария обещанных платежей

							</li>
					
						<li>
						
						Интеграция платежного сервиса с возможностью совершения платежей онлайн	</li>
					</ul>
					
				
				
				</div>
				
				<div class="col-md-6">
					<ul class="main-ul">
					
						<li>
						Легкий доступ клиента к данным личного кабинета информация о задолженности и возможности погашения 

						
						</li>
						
						<li>
						Мультимедиа взаимодействие: баннеры, видео, фото, динамические документы / досудебные претензии

						
						</li>
						
						<li>
							С2О с применением голосового сервиса (+IVR), меняет сценарий информирования в зависимости от действий заемщика

						</li>
						
					
					</ul>	
				</div>
				
				<div class="clear"></div>
				<hr>
				<div class="col-md-12">
				<p class="text-right blue-text"><a href="<?php Url::base(''); ?>/microkredit/">
				
				Узнать подробнее 
				<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 62.1 116.09"><defs><style>.cls-1{fill:#0062ff;}</style></defs><title>Asset 1</title><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><path class="cls-1" d="M1.2,1.19A4.1,4.1,0,0,0,1.2,7L52.3,58,1.2,109.09a4.1,4.1,0,1,0,5.8,5.8L60.9,61a4,4,0,0,0,1.2-2.9,4.18,4.18,0,0,0-1.2-2.9L7,1.29A4,4,0,0,0,1.2,1.19Z"/></g></g></svg>
				
				
				</a></p>
				</div>
			</div>
			
			
		</div>
		
		<div class="col-md-6 mob-none">
			<img style="width: 100%;" src="images/5.png">
		</div>	
	</div>

</div>	
</section>	




<section id="" class="how-work-sec">
	<div class="container">
	<div class="row">
		<div class="container">
			<h2>Как работает Click to Offer</h2>
		</div>
		</div>
	</div>
	
	
	
	<div class="bg-siren">
	
		<div class="container">
	
		<div class="row">
			
				
				<div class="">
				
				
					<div class="col-md-4">
						
						<div class="steps">
						<span class="st-wr">Шаг 1</span>
						
						<div class="img-wrap-step">
						
						<img style="width: 100%;" src="images/step_1.png">
						</div>	
						
						
						
						
						
						</div>
						
						<div class="step-text">
							
							<p>
							Клиент получает SMS-сообщение, содержащее приглашение перейти в сервис 
							по персональной ссылке, сгенерированной для него, и ознакомится с ним.
							
							
							</p>
							
						</div>
						
						
					</div>
					<div class="col-md-4">
					<div class="steps">
						<span class="st-wr">Шаг 2</span>
						<div class="img-wrap-step">
						<img style="width: 100%;" src="images/step_2.png">
						</div>
						
						
						
					</div>
					
					
					<div class="step-text">
							
							<p>
							Перейдя по ссылке, клиент через браузер мобильного устройства попадает в сервис. 
								И взаимодействует с ним в соответствии с заложенным функционалом.

							
							</p>
							
						</div>
						
					
					
					</div>
					
					<div class="col-md-4">
					<div class="steps">
						<span class="st-wr">Шаг 3</span>
						<div class="img-wrap-step wide">
						<img style="width: 100%;" src="images/step_3.png">
						</div>
						
						
						
						
					</div>
					
					<div class="step-text">
							
							<p>
							
							После рассылок и действий клиентов в сервисе вы можете проанализировать детальный отчет, сформированный в нашей системе, по действиям клиентов в сервисе.

							
							</p>
							
						</div>
					
					
					</div>
				</div>
			
			</div>
			
		</div>		
			
	</div>
	



</section>	
	
	
	
	
<section class="carousel-sec">
		
	
<div class="container">


<div class="row">

<div class="container">
<h2>Наши партнеры</h2>

</div>
<br>

<?php
use kv4nt\owlcarousel\OwlCarouselWidget;

OwlCarouselWidget::begin([
    'container' => 'div',
    'assetType' => OwlCarouselWidget::ASSET_TYPE_CDN,
    'containerOptions' => [
        'id' => 'container-id',
        'class' => 'container-class owl-theme'
    ],
    'pluginOptions'    => [
        'autoplay'          => true,
        'autoplayTimeout'   => 5000,
        'items'             => 5,
        'loop'              => true,
        'itemsDesktop'      => [1100, 3],
        'itemsDesktopSmall' => [979, 3],
		'nav' => 'true',
    //navText : ["",""],
    ]
]);
?>

<div class="item-class" ><img src="images/mtsabank_new_logo.svg" alt="Image 1"></div>
<div class="item-class"><img src="images/logo_orig.svg" alt="Image 2"></div>
<div class="item-class"><img src="images/a.svg" alt="Image 3"></div>
<div class="item-class"><img src="images/header-logo-slogan.svg" alt="Image 4"></div>
<div class="item-class"><img src="images/pik-komf.svg" alt="Image 5"></div>



<?php OwlCarouselWidget::end(); ?>

</div>	
</div>	
	
	
	
</section>





	
</div>
