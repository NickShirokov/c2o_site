<?php

/* @var $this yii\web\View */


$this->title = 'Click to Offer - высокотехнологичная платформа для
	   коммуникации, информирования и сбора задолженностей с ваших клиентов';
?>



<div class="site-index">

<section id="" class="first-banner">


    <div class="container flex-center-align">

    <div class="jumbotron">
       

	   <h1>Предприятия ЖКХ и поставщики энергоресурсов</h1>

	   <p class="lead">
	   Эффективные инструменты по информированию должников и взысканию задолженности по квартплате и коммунальным услугам
	   </p>

	   <img style="width: 50%;" class="" src="../images/2.png">

	   </div>

	</div>
	
</section>	
	
	
	
<!-- ВТорая секция -->	
	
<section id="" class="second-sec">	
<div class="container">	
	
	
	<div class="row">

	
		<div class="col-md-8">

			<p class="blue-text">Решение</p>
			<h2>Предприятия ЖКХ и поставщики воды и энергоресурсов
			</h2>
			
			<p class="lead">
			Эффективные инструменты по информированию потребителей услуг, формирование платежной дисциплины, дистанционная оплата, удобная всем категориям потребителей, электронные квитанции по любым удобным каналам, удобные и простые формы платежей

		</div>
		
		
		<div class="col-md-4 img-box-wr">
			<img  src="../images/JKH_1.png">		   
	   <br>
	   <br>
	   
	   	
	   
	   
		</div>
		
		
		
		
		<div class="col-md-12">
		
			<div class="row">
			
				<div class="col-md-4">
				
						
					<div class="clear"></div>
					<hr>
				
					
					<ul class="main-ul">
						<li>
						Информирование потребителей в соответствии с Законодательством РФ, предоставляющее Поставщику услуг возможность осуществлять коммуникацию без предварительного согласия на использование персональных данных клиента
</li>
					
						<li>
							Удобные форматы дистанционных платежей. Сценарии рассрочки, прощения пеней, штрафов
						</li>
					
						
					</ul>
					
					<div class="clear"></div>
				<hr>
				
				
				</div>
				
				<div class="col-md-4">
				
						
					<div class="clear"></div>
					<hr>
				
					<ul class="main-ul">
					
						<li>
						Легкий доступ клиента к данным личного кабинета информация о задолженности и возможности погашения

						
						</li>
						
						<li>
							Мультимедиа взаимодействие: баннеры, видео, фото, динамические документы / досудебные претензии

						
						</li>
						
						
						
						
					
					</ul>	
					
					<div class="clear"></div>
				<hr>
					
				</div>
				
				
				
				<div class="col-md-4">
				
						
					<div class="clear"></div>
				<hr>
				
					<ul class="main-ul">
					
						<li>
							Адаптация экранных форм под запросы любых категорий пользователей, включая людей с ограниченными возможностями

						</li>
					
					
				
						
						<li>
							С2О с применением голосового сервиса (+IVR), меняет сценарий информирования в зависимости от действий заемщика

						
						</li>
						
					
					</ul>	
					
					<div class="clear"></div>
				<hr>
					
				</div>
				
				
			
			</div>
			
			
		</div>
	
	
	<div class="clear"></div>
	
	<br>
	<div class="col-md-12">
	<h2>Оставьте заявку на Click to Offer <span  class="zayavka blue-zayavka">
			Заказать
		</span></h2> 
	</div>
	
	</div>
	
	
	
</div>	
</section>	
	
	
	

	
</div>
