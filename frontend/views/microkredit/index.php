<?php

/* @var $this yii\web\View */


$this->title = 'Click to Offer - высокотехнологичная платформа для
	   коммуникации, информирования и сбора задолженностей с ваших клиентов';
?>



<div class="site-index">

<section id="" class="first-banner">


    <div class="container flex-center-align">

    <div class="jumbotron">
       

	   <h1>Микрофинансовые организации</h1>

	   <p class="lead">Полноценный контроль собираемости задолжностей и система информирования групп с приближающимся сроком выплаты по кредиту.</p>


	   <img style="width: 50%;" class="" src="../images/5.png">


	   </div>

	
	</div>
	
</section>	
	
	
	
<!-- ВТорая секция -->	
	
<section id="" class="second-sec">	
<div class="container">	
	
	
	<div class="row">
	<!--
		<div class="col-md-6">
			<img style="width: 75%;" src="images/2.png">
		</div>
	-->
	
		<div class="col-md-8">
			
			
			<p class="blue-text">Решение</p>
			<h2>Кредитные организации. МФО</h2>
			
			<p class="lead">
			Полноценный контроль собираемости задолжностей и система информирования групп с приближающимся сроком выплаты по кредиту.</p>
		
		</div>
		
		
		<div class="col-md-4 img-box-wr">
			<img src="../images/bank_1.png">
			
				   
	   <br>
	   <br>
	   
	   	
	   
	   
		</div>
		
		
		
		
		<div class="col-md-12">
		
			<div class="row">
			
				<div class="col-md-4">
				
						
					<div class="clear"></div>
					<hr>
				
					
					<ul class="main-ul">
						<li>Полное соответствие требованиям правилами предоставления коммунальных услуг № 354, от 06.05.2011 г.
						</li>
					
						<li>
							Инструменты реструктуризации долга, пролонгации, реализация сценария обещанных платежей
						</li>
					
						
					</ul>
					
					<div class="clear"></div>
				<hr>
				
				
				</div>
				
				<div class="col-md-4">
				
						
					<div class="clear"></div>
					<hr>
				
					<ul class="main-ul">
					
						<li>
						
							Интеграция платежного сервиса с возможностью совершения платежей онлайн
						</li>
					
					
						<li>
						Легкий доступ клиента к данным личного кабинета информация о задолженности и возможности погашения


						
						</li>
						
						
					
					</ul>	
					
					<div class="clear"></div>
				<hr>
					
				</div>
				
				
				
				<div class="col-md-4">
				
						
					<div class="clear"></div>
				<hr>
				
					<ul class="main-ul">
					
						
						
						<li>
							Мультимедиа взаимодействие: баннеры, видео, фото, динамические документы / досудебные претензии

						
						</li>
						
						<li>
							С2О с применением голосового сервиса (+IVR), меняет сценарий информирования в зависимости от действий заемщика

						
						</li>
						
					
					</ul>	
					
					<div class="clear"></div>
				<hr>
					
				</div>
				
				
			
			</div>
			
			
		</div>
	
	
	<br>
	
	<div class="col-md-12">
	<h2>Оставьте заявку на Click to Offer <span  class="zayavka blue-zayavka">
			Заказать
		</span></h2> 
	</div>
	
	</div>
	
	
	
</div>	
</section>	
	
	
	

	
</div>
