<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>
	
	<?= $form->field($model, 'username')->textInput() ?>
	
	<?= $form->field($model, 'email')->textInput() ?>
	
	
	<?//= $form->field($model, 'password')->textInput() ?>
	
	
	<?//= $form->field($model, 'password')->passwordInput() ?>
	
	<div style="display: none">
    <?= $form->field($model, 'status')->textInput() ?>
	</div>
	
	 <?= $form->field($model, 'type')->dropdownList(['admin' => 'admin', 'manager' => 'manager', 'payer' => 'payer', 'user' => 'user' ]) ?>
	

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Сохранить'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
