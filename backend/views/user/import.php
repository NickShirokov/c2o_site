<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;


use yii\widgets\ActiveForm;

use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\UserSearchModel */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Импорт данных');



$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>
   
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    

		<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

		
		<h2>Формы загрузки данных владельцев</h2>
	
		<label>
		<input type="checkbox" name="remove">
		Удалить данные
		</label>
		
		
		<div class="form-group field-ownersfile">
		<label class="control-label" for="ownersfile">Файл CSV</label>
		<input type="hidden" name="Owners[file_f]" value="">
		<input type="file" id="ownersfile" name="Owners[file_f]">

		<div class="help-block"></div>
		</div>
		
	 
		<div style="clear: both;">
	 
		<div class="form-group">
			<?= Html::submitButton(Yii::t('app', 'Загрузить'), ['class' => 'btn btn-success']) ?>
		</div>

		<?php ActiveForm::end(); ?>
		
		
		
		
		<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

		
		<h2>Формы загрузки данных лицензий</h2>
	
		<label>
		<input type="checkbox" name="remove">
		Удалить данные
		</label>
		
		
		<div class="form-group field-lic-file_f">
		<label class="control-label" for="lic-file_f">Файл CSV</label>
		<input type="hidden" name="Licen[file_f]" value="">
		<input type="file" id="lic-file_f" name="Licen[file_f]">

		<div class="help-block"></div>
		</div>
		
	 
		<div style="clear: both;">
	 
		<div class="form-group">
			<?= Html::submitButton(Yii::t('app', 'Загрузить'), ['class' => 'btn btn-success']) ?>
		</div>

		<?php ActiveForm::end(); ?>
		
		
		<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

		
		<h2>Формы загрузки данных страховых</h2>
	
		<label>
		<input type="checkbox" name="remove">
		Удалить данные
		</label>
		
		
		<div class="form-group field-ins-file_f">
		<label class="control-label" for="ins-file_f">Файл CSV</label>
		<input type="hidden" name="Insurance[file_f]" value="">
		<input type="file" id="ins-file_f" name="Insurance[file_f]">

		<div class="help-block"></div>
		</div>
		
	 
		<div style="clear: both;">
	 
		<div class="form-group">
			<?= Html::submitButton(Yii::t('app', 'Загрузить'), ['class' => 'btn btn-success']) ?>
		</div>

		<?php ActiveForm::end(); ?>
		
		
		<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

		
		<h2>Формы загрузки данных марки</h2>
	
		<label>
		<input type="checkbox" name="remove">
		Удалить данные
		</label>
		
		
		<div class="form-group field-marks-file_f">
		<label class="control-label" for="marks-file_f">Файл CSV</label>
		<input type="hidden" name="Marks[file_f]" value="">
		<input type="file" id="marks-file_f" name="Marks[file_f]">

		<div class="help-block"></div>
		</div>
		
	 
		<div style="clear: both;">
	 
		<div class="form-group">
			<?= Html::submitButton(Yii::t('app', 'Загрузить'), ['class' => 'btn btn-success']) ?>
		</div>

		<?php ActiveForm::end(); ?>
		
		
		
		<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

		
		<h2>Формы загрузки данных модели</h2>
	
		<label>
		<input type="checkbox" name="remove">
		Удалить данные
		</label>
		
		
		<div class="form-group field-models-file_f">
		<label class="control-label" for="models-file_f">Файл CSV</label>
		<input type="hidden" name="Models[file_f]" value="">
		<input type="file" id="models-file_f" name="Models[file_f]">

		<div class="help-block"></div>
		</div>
		
	 
		<div style="clear: both;">
	 
		<div class="form-group">
			<?= Html::submitButton(Yii::t('app', 'Загрузить'), ['class' => 'btn btn-success']) ?>
		</div>

		<?php ActiveForm::end(); ?>
		
		
		
		
		
		<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

		
		<h2>Формы загрузки данных Статусы водителей</h2>
	
		<label>
		<input type="checkbox" name="remove">
		Удалить данные
		</label>
		
		
		<div class="form-group field-automobilefiles-file_f">
		<label class="control-label" for="automobilefiles-file_f">Файл CSV</label>
		<input type="hidden" name="Owners[file_f]" value="">
		<input type="file" id="automobilefiles-file_f" name="Owners[file_f]">

		<div class="help-block"></div>
		</div>
		
	 
		<div style="clear: both;">
	 
		<div class="form-group">
			<?= Html::submitButton(Yii::t('app', 'Загрузить'), ['class' => 'btn btn-success']) ?>
		</div>

		<?php ActiveForm::end(); ?>
		
		
		
		
		<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

		
		<h2>Формы загрузки данных Статусы Машин</h2>
	
		<label>
		<input type="checkbox" name="remove">
		Удалить данные
		</label>
		
		
		<div class="form-group field-automobilefiles-file_f">
		<label class="control-label" for="automobilefiles-file_f">Файл CSV</label>
		<input type="hidden" name="Owners[file_f]" value="">
		<input type="file" id="automobilefiles-file_f" name="Owners[file_f]">

		<div class="help-block"></div>
		</div>
		
	 
		<div style="clear: both;">
	 
		<div class="form-group">
			<?= Html::submitButton(Yii::t('app', 'Загрузить'), ['class' => 'btn btn-success']) ?>
		</div>

		<?php ActiveForm::end(); ?>
		
		
		
	

   
</div>
