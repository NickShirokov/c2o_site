<?php

$config = [
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'DpcN7CbUEnqn_8r-pkYiU3NlXzQlZopS',
        ],
		
		
		'formatter' => [
			'dateFormat' => 'd.m.y',
			'decimalSeparator' => ',',
			'thousandSeparator' => ' ',
			'currencyCode' => 'EUR',
	   ],

		
    ],
];

//print_r (YII_ENV_DEV);

//exit;


if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        'allowedIPs' => ['127.0.0.1', '5.101.152.56'],
    ];
}

return $config;
