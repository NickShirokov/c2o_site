<?php
return [
    'language' => 'ru',
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
		
		 'authManager' => [
            'class' => 'yii\rbac\DbManager',
        ],
		
		
		/*'assetManager' => [
		    'bundles' => [
			    'yii\web\JqueryAsset' => [
				    'js'=>[]
			    ],
			    'yii\bootstrap\BootstrapPluginAsset' => [
				    'js'=>[]
			    ],
			    'yii\bootstrap\BootstrapAsset' => [
				    'css' => [],
			    ],

		    ],
	    ],
		*/
		/*
		
		'assetManager' => [
			'bundles' => [
				
				'yii\bootstrap\BootstrapAsset' => [
				    'css' => [],
			    ],
			
			
				'yii\bootstrap4\BootstrapAsset' => [
					'sourcePath' => '@npm/bootstrap/dist'
				],
				'yii\bootstrap4\BootstrapPluginAsset' => [
					'sourcePath' => '@npm/bootstrap/dist'
				],
				'yii\bootstrap4\BootstrapThemeAsset' => [
					'sourcePath' => '@npm/bootstrap/dist'
				],
				'yii\bootstrap4\PopperAsset' => [
					'sourcePath' => '@npm/popper.js/dist/umd'
				], 
			]
		],
		*/
		
		
    ],
	
	
		
	'modules' => [
		'gridview' => [
			'class' => 'kartik\grid\Module',
			// other module settings
		]
	],
	
	
	
];
