<?php
return [
    'components' => [
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=localhost;dbname=c2o_local',
            'username' => 'root',
            'password' => '',
            'charset' => 'utf8',
        ],
		
	
		'formatter' => [
			'dateFormat' => 'd.m.y',
			'decimalSeparator' => ',',
			'thousandSeparator' => ' ',
			'currencyCode' => 'EUR',
	   ],

		
		
		'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@common/mail',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => false,
			
	
	
			'transport' => [
				'class' => 'Swift_SmtpTransport',
				'host' => 'ssl://smtp.yandex.ru',
				'username' => 'info@c2o.me',
				'password' => 'Broomks@df42',
				'port' => '465',
				//'encryption' => 'tls',

			],


			
        ],
	
		
		
    ],
];
